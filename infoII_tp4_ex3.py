class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item, priority):  # Add an item to the queue with a priority 0 being the lowest priority
    priority = int(priority)            # Convert the priority to an integer
    if self.is_empty():
        self.items.append((priority, item))          # If the queue is empty, add the item
    else:
        for i in range(len(self.items)):
            if priority > self.items[i][0]:             # If the queue is not empty, add the item at the right index
                self.items.insert(i, (priority, item))
                return
        self.items.append((priority, item))

    def dequeue(self):              # Remove the item with the lowest priority, it is the last item in the list because of the way we add items
    if not self.is_empty(): 
        return self.items.pop()
    return None

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

    def afficher(self):         # Display the items in the queue
    if self.is_empty():
        print("La file est vide")
    else:
        print("La file est: ")
        for priority, item in self.items:
            print(f"({priority}, {item})")


if __name__ == '__main__':
    file = File()
    while True:     # Quick menu for better user interaction
        print("\nMenu:")
        print("1) Ajouter un élément")
        print("2) Retirer un élément")
        print("3) Afficher")
        print("4) Quitter")

        choix = input("\nChoisissez une option>>> ")

        if choix == "1":
            element = input("Entrez l'élément à ajouter: ")
            priority = int(input("Entrez l'index de priorité: "))
            file.enqueue(element, priority)
        elif choix == "2":
            file.dequeue()
            print("L'élément le moins prioritaire a été retiré")
        elif choix == "3":
            file.afficher()
        elif choix == "4":
            break
